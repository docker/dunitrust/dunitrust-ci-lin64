FROM buildpack-deps:stretch
MAINTAINER elois <elois@ifee.fr>
LABEL version="1.41"
LABEL description="CI for DURS project (Divende Universel RuSt)"

RUN apt-get update && \
   apt-get install -y build-essential curl python-pip pkg-config libssl-dev sudo tar zip && \
   apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
   
# install rustup
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# rustup directory
ENV PATH /root/.cargo/bin:$PATH


# Install fmt and clippy
RUN rustup component add rustfmt-preview
RUN rustup component add clippy

# install cargo deb and cargo deny
RUN cargo install cargo-deb && cargo install cargo-deny